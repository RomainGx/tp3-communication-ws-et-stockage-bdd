package fr.isima.android.tp3.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import fr.isima.android.tp3.database.DatabaseHelper;
import fr.isima.android.tp3.database.TagTable;

public class TagProvider extends ContentProvider
{
	private static final int TAGS = 1;
	private static final int TAG_ID = 2;

	private static final UriMatcher URI_MATCHER;
	
	public static final String AUTHORITY = "fr.isima.android.tp3.provider.TagProvider";
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/tags");

    private DatabaseHelper db;

	static
	{
		URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
		URI_MATCHER.addURI(AUTHORITY, "tags", TAGS);
		URI_MATCHER.addURI(AUTHORITY, "tag/#", TAG_ID);
	}


	@Override
	public boolean onCreate()
	{
		db = DatabaseHelper.getInstance(getContext());
		return db != null;
	}
	
	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
	{
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
		queryBuilder.setTables(TagTable.TABLE_NAME);
		
		Cursor cursor = queryBuilder.query(db.getReadableDatabase(), projection, selection, selectionArgs, null, null, sortOrder);
	    cursor.setNotificationUri(getContext().getContentResolver(), uri);
		
		return cursor;
	}
	
	@Override
	public Uri insert(Uri uri, ContentValues values)
	{
		long id = db.getWritableDatabase().insert(TagTable.TABLE_NAME, null, values);
		
		if (id > 0)
		{
			Uri rowUri = ContentUris.withAppendedId(CONTENT_URI, id);
			getContext().getContentResolver().notifyChange(rowUri, null, false);
			
			return rowUri;
		}
		
		throw new SQLException("Failed to insert row into " + uri);
	}
	
	@Override
	public int update(Uri uri, ContentValues values, String where, String[] whereArgs)
	{
		int count = db.getWritableDatabase().update(TagTable.TABLE_NAME, values, where, whereArgs);
		getContext().getContentResolver().notifyChange(uri, null, false);
		
		return count;
	}
	
	@Override
	public int delete(Uri uri, String where, String[] whereArgs)
	{
		int count = db.getWritableDatabase().delete(TagTable.TABLE_NAME, where, whereArgs);
		getContext().getContentResolver().notifyChange(uri, null, false);
		
		return count;
	}
	
	@Override
	public String getType(Uri uri)
	{
		switch (URI_MATCHER.match(uri))
		{
			case TAGS:
				return "vnd.isimaandroid.cursor.dir/tag";
			case TAG_ID:
				return "vnd.isimaandroid.cursor.item/tag";
			default:
				return null;
		}
	}
}
